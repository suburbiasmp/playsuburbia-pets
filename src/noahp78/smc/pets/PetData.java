package noahp78.smc.pets;

import java.util.HashMap;

import net.citizensnpcs.api.npc.NPC;

public class PetData {
	public HashMap<String,NPC>Pets = new HashMap<String,NPC>();
	public NPC GetPet(String name){
		return Pets.get(name);
	}
	public void AddPet(String name, NPC pet){
		Pets.remove(name);
		pet.destroy();
		Pets.put(name, pet);
	}
}
