package noahp78.smc.pets;

import java.io.Serializable;

import net.citizensnpcs.api.npc.NPC;

public class Pet implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String owner;
	public String name;
	public int type;
	
}
