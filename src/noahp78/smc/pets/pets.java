package noahp78.smc.pets;

import java.util.HashMap;

import net.aufdemrand.sentry.Sentry;
import net.aufdemrand.sentry.SentryTrait;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.EntityEffect;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;


public class pets extends JavaPlugin implements Listener{
	public static HashMap<String,NPC>Pets = new HashMap<String,NPC>();
	@Override
	public void onEnable() {
		System.out.println("Opening SQL connection for pets!");
		SqlHelper.PreInitSQL();
		getServer().getPluginManager().registerEvents(this, this);

	}
	@Override
	public void onDisable(){
		System.out.println("Saving petData!");

	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("pets") || cmd.getName().equalsIgnoreCase("pet")) { // If the player typed /basic then do the following...
			if (!(sender instanceof Player)) {
				sender.sendMessage("This command can only be run by a player.");
			} else {
				if(args.length==0){
					sender.sendMessage(ChatColor.GOLD+ "[PETS]" + ChatColor.GREEN + " Pets Plugin made by noahp78");
					sender.sendMessage(ChatColor.GOLD + "/pet store" + ChatColor.GREEN +  "Open the Pet store");
					sender.sendMessage(ChatColor.GOLD+ "/pet name [name]" + ChatColor.GREEN + " sets the name of your current pet");
					sender.sendMessage(ChatColor.GOLD+ "/pet remove" + ChatColor.GREEN+" kills your current pet. How mean!");
					return true;
				}else{
				final Player player = (Player) sender;
				if(args[0].equalsIgnoreCase("spawn")){
					if(args.length==2){
						System.out.println(args[1]);
						String b = args[1];
						Integer a = Integer.parseInt(b);
						if(Pets.get(player.getCustomName())!=null){
							player.sendMessage("Sorry. You already have a pet!");
						}else{
						CreateNPCid(player,a,ChatColor.GOLD + "[pet] " + ChatColor.GREEN + player.getDisplayName());
						}
					}else{
						player.sendMessage("INCORRECT USAGE!");
					}
					return true;
				}else if (args[0].equalsIgnoreCase("name")){
					if(args.length==2){
						
						String Name = ChatColor.GOLD + "[PET] " + ChatColor.GREEN + args[1];
						
						NPC pet = Pets.get(player.getDisplayName());
						pet.setName(Name);
					}else{
						sender.sendMessage("Please also add a name for your pet!");
					}
				}else if (args[0].equalsIgnoreCase("remove")){
					player.sendMessage("~ You look away ~");
							player.playSound(player.getLocation(), "mob.enderdragon.hit", 1F, 1F);
							NPC pet = Pets.get(player.getDisplayName());
							pet.getBukkitEntity().playEffect(EntityEffect.DEATH);
							pet.destroy();
				}else if(args[0].equalsIgnoreCase("store")){
					//TODO open pet store menu!
					PetStore.update();
					player.openInventory(PetStore.GameINV);
				}
				// do something
			}
			return true;
		}
		}
		return false;
		
	}
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onLeave(PlayerQuitEvent e){
	String playername = e.getPlayer().getDisplayName();
	Pets.get(playername).destroy();
		
	}
	@EventHandler(priority=EventPriority.LOWEST)
	public void onPlayerJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		try {
			SqlHelper.SpawnUserPet(p);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	@EventHandler(priority=EventPriority.HIGHEST)
	public void EntityDamage(EntityDamageEvent e){
		e.setCancelled(true);
	}
	public static void CreateNPC( Player p, EntityType type, String name){
		NPC npc = CitizensAPI.getNPCRegistry().createNPC(type, name);
		npc.spawn(p.getLocation());
		npc.addTrait(SentryTrait.class);
		npc.teleport(p.getLocation(), TeleportCause.PLUGIN);
		Sentry a = (Sentry) Bukkit.getPluginManager().getPlugin("Sentry");
		a.getSentry(npc).setGuardTarget(p.getDisplayName(), true);
		a.getSentry(npc).sentryHealth = 2000.0;
		npc.setProtected(true);
		pets.Pets.put(p.getDisplayName(),npc);
	}
	public static void CreateNPCid(Player p, int id, String name){
		EntityType e = GetEntityType(id);
		if (e.equals(EntityType.PLAYER)){
			//TODO CODE IN NAMES
		}else{
			CreateNPC(p,e,name);
		}
	}
	public static EntityType GetEntityType(int id){
		if(id==0){
			return null;
			//SHOULD NEVER BE CALLED!
		}else if (id==1){
			return EntityType.ZOMBIE;
		}else if (id==2){
			return EntityType.OCELOT;
		}else if (id==3){
			return EntityType.SHEEP;
		}else if (id==4){
			return EntityType.CHICKEN;
		}else if(id==5){
			return EntityType.SKELETON;
		}else if(id==6){
			return EntityType.COW;
		}else if(id==7){
			return EntityType.PIG;
		}else if(id==8){
			return EntityType.WOLF;
			//TAMED
		}else if(id==9){
			return EntityType.CREEPER;
		}else if (id==10){
			return EntityType.MUSHROOM_COW;
		}else if(id==11){
			return EntityType.SLIME;
		}else if(id==12){
			return EntityType.HORSE;
		}else if(id==13){
			return EntityType.BAT;
		}else if(id==14){
			return EntityType.SNOWMAN;
		}else if (id==15){
			return EntityType.OCELOT;
			//TAMED
		}else if(id==16){
			return EntityType.SPIDER;
		}else if(id==17){
			return EntityType.VILLAGER;
		}else if(id==18){
			return EntityType.IRON_GOLEM;
		}else if(id==19){
			return EntityType.SKELETON;
			//WITHER
		}else if(id==20){
			return EntityType.WITCH;
		}else if(id==21){
			return EntityType.BLAZE;
		}else if(id==22){
			return EntityType.PIG_ZOMBIE;
		}else if(id==23){
			return EntityType.SILVERFISH;
		}else if(id==24){
			return EntityType.CAVE_SPIDER;
		}else if(id==25){
			return EntityType.MAGMA_CUBE;
		}else if(id>25){
			if(id<54){
				return EntityType.PLAYER;
			}
		}else{
			return null;
		}
		return null;
	}

}
